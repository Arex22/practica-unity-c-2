﻿using UnityEngine;

public class RaycastExample : MonoBehaviour {

	public GameObject esfera;
    [Range(0.01f, 250)]
    public float distance = 100;

	void FixedUpdate() {

		esfera = GameObject.Find("paraSpawner");
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;

		if (Input.GetButton("Fire2"))
		{
			if (Physics.Raycast(transform.position, fwd, out hit, distance))
			{
				Destroy(esfera);
			}
		}
    }
}

