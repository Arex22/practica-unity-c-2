﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleMovement : MonoBehaviour {


	void Update () {

		Vector3 rotador=new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
		float movHorizontal, movVertical;
		
		movHorizontal = Input.GetAxisRaw("Horizontal"); 
		movVertical = Input.GetAxisRaw("Vertical");  

		// Vertical Movement (Z axis)
		if (movVertical != 0)
		{
			transform.Translate(movVertical * Vector3.forward * Time.deltaTime *20); 
		}

		// Horizontal Movement (X axis)
		if (movHorizontal != 0)
		{
			transform.Translate(movHorizontal * Vector3.right * Time.deltaTime * 20);
		}

		//para el giro con el raton
		transform.Rotate(rotador * Time.deltaTime * 20);
	}


	void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts[0];
		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		Vector3 pos = contact.point;
		Destroy(gameObject);
	}
}
