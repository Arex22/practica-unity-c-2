﻿using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject objectToSpawn;
	public float frecuencia;
	public Color color;

	private void Start()
	{
		InvokeRepeating("Creacion", 0, frecuencia);
	}


	void Update () {

			

	}

	void Creacion()
	{
		GameObject objAux = Instantiate(objectToSpawn, transform.position, Quaternion.identity) as GameObject;
		float scale = Random.Range(1, 10);
		objAux.transform.localScale = new Vector3(scale, scale, scale);

		Rigidbody rb = objAux.GetComponent<Rigidbody>();

		rb.drag = Random.Range(0f, 0.1f);

		rb.mass = Random.Range(1, 25);

		rb.AddForce(Vector3.right * Random.Range(10, 150), ForceMode.Impulse);

		objAux.GetComponent<MeshRenderer>().material.color = color;
	}
}

