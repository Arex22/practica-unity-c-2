﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerMovement : MonoBehaviour {

	GameObject CentroDeGiro;
	public Transform centro;
	
	
	public float radio ;
	
	

	void Start(){
		CentroDeGiro = GameObject.Find("CentroDeGiro");
		centro = CentroDeGiro.transform;
		//asignamos la posición inicial normalizando el vector, multiplicándolo por el radio y sumándole la posición del centro
		transform.position = (transform.position - centro.position).normalized * radio + centro.position;

	
	}

	void Update(){

		float velGiro = 80;
		Vector3 ejeObjeto = new Vector3(0,transform.position.y,0);

		transform.RotateAround(centro.position, ejeObjeto, velGiro * Time.deltaTime);

		transform.position= (transform.position - centro.position).normalized * radio + centro.position;


	}
}
