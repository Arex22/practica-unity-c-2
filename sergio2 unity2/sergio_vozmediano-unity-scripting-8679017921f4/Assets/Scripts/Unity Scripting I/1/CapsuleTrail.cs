﻿using UnityEngine;

public class CapsuleTrail : MonoBehaviour {

    private TrailRenderer trail;

    // With Range it possible to limit the values that variables can take
    [Range(0.1f, 50)]
    public float startWidht = 5;
    [Range(0.1f, 50)]
    public float endWidht = 2;

    private void Start() {
        // Getting component's reference present in the same GameObject
        if (!(trail = GetComponent<TrailRenderer>())) {
            Debug.LogError("No TrailRenderer Component attached to the GameObject.");
            enabled = false;
            return;
        }
    }
	
	// Update is called once per frame
	void Update () {
        //Accessing trailRenderer component present in the same GameObject
        trail.time = Random.Range(1, 100);
        trail.startWidth = startWidht;
        trail.endWidth = endWidht;
	}
}


