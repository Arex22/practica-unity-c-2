﻿using UnityEngine;

public class CapsuleRotation : MonoBehaviour {

    // Degress to rotate
    public float rotationDegrees = 5f;

	// Use this for initialization
	void Start () {
        // Rotate rotationDegrees per second along y axis (Only first frame)
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotating From Start Event");
	}
	
	// Update is called once per frame
	void Update () {
        // Rotate rotationDegrees per second along y axis (executed every frame)
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotating From Update Event");
    }
}


